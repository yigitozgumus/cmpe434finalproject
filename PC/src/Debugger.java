import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;

public class Debugger extends JFrame {

	private static final long serialVersionUID = 3100267790431833118L;
	private static final int lateral = 5;
	private static final int vertical = 5;
	
	
	//Bit order
	//
	// bottom - right - top - left // reverse order 
	//
	static InputStream inputStream;
	static DataInputStream dataInputStream;
	static ArrayList<Line2D> map =new ArrayList<>();
	static ArrayList<Line2D> cells =new ArrayList<>();
	static ArrayList<Rectangle2D> black = new ArrayList();
	static ArrayList<Rectangle2D> blue = new ArrayList();
	static ArrayList<Rectangle2D> red = new ArrayList();
	static Point robot = new Point();
	static boolean robotHere = false;
	public Debugger() {
		super("CmpE 434");
		setSize( 600, 625 );
		setVisible( true );
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initializeMap();
		repaint();
	}
	
	public static void main(String[] args) throws UnknownHostException, IOException {
		Debugger monitor = new Debugger();
		String ip = "192.168.43.218";
		@SuppressWarnings("resource")
		Socket socket = new Socket(ip,666);
		System.out.println("Connected!");

		inputStream = socket.getInputStream();
		dataInputStream = new DataInputStream(inputStream);
		while(true){
			int x = dataInputStream.readInt();
			int y = dataInputStream.readInt();
			int wall = dataInputStream.readInt();
			System.out.println("x: "+x+" y: "+y+" wall: "+wall);
			monitor.addCell(wall, y, x);
			monitor.displayRobot(y, x);
			monitor.repaint();
		}
		
	}
	
	public void paint( Graphics g ) {
		super.paint( g );
		if(robotHere){
			showRobot(g);
		}
		displayMap( map, g);
	}
	private void showRobot(Graphics g) {
		// TODO Auto-generated method stub
		int xLoc = (2 * robot.x -1) * 25;
		int yLoc = ((2 * robot.y -1)* 25) +25;
		Ellipse2D robot = new Ellipse2D.Float(xLoc, yLoc, 10, 10);
		Graphics2D gogo = (Graphics2D) g;
		gogo.setColor(Color.YELLOW);
		gogo.setStroke(new BasicStroke(6.0f));
		gogo.draw(robot);
	}

	public void initializeMap(){
		for(int i = 0;i <12;i++){
			map.add(new Line2D.Float(0,((i) * 50)+25,600,((i) * 50)+25));
			map.add(new Line2D.Float((i) * 50,25,(i) * 50,625));
		}
	}

	
	public void displayRobot(int x,int y){
		robotHere = true;
		robot.x = x;
		robot.y = y;
	}
	
	public void addCell(int num,int x,int y){
		int xLoc = (x-1) * 50 ;
		System.out.println("xLoc: "+ xLoc);
		int yLoc = ((y-1) * 50 )+25;
		System.out.println("yLoc: "+ yLoc);
		//Dirty
		if(getBit(num,0)){//left
			cells.add(new Line2D.Float(xLoc,yLoc,xLoc,yLoc+50));
		}if(getBit(num,1)){//top
			cells.add(new Line2D.Float(xLoc,yLoc,xLoc+50,yLoc));
		}if(getBit(num,2)){//right
			cells.add(new Line2D.Float(xLoc+50,yLoc,xLoc+50,yLoc+50));
		}if(getBit(num,3)){//bottom
			cells.add(new Line2D.Float(xLoc,yLoc+50,xLoc+50,yLoc+50));

		}if(getBit(num,4)){//red
			red.add(new Rectangle2D.Float(xLoc,yLoc,50,50));
		}if(getBit(num,5)){//blue
			blue.add(new Rectangle2D.Float(xLoc,yLoc,50,50));
		}if(getBit(num,6)){//black
			black.add(new Rectangle2D.Float(xLoc,yLoc,50,50));
		}
		
	}
	
	
	public void displayMap( ArrayList<Line2D> map, Graphics g ){
		Graphics2D g2 = ( Graphics2D ) g;
		g2.setPaint( Color.BLACK );
		g2.setStroke( new BasicStroke( 1.0f ));

		for ( int i = 0; i < map.size(); i++ ){
			g2.draw(map.get(i));
		}
		g2.setPaint( Color.BLACK );
		g2.setStroke( new BasicStroke( 5.0f ));
		for ( int i = 0; i < cells.size(); i++ ){
			g2.draw(cells.get(i));
		}
		g2.setPaint( Color.BLACK );
		g2.setStroke( new BasicStroke( 5.0f ));
		for ( int i = 0; i < black.size(); i++ ){
			g2.drawRect((int)black.get(i).getX()+10,(int)black.get(i).getY()+10,(int)black.get(i).getWidth()-20,(int)black.get(i).getHeight()-20);
		}
		g2.setPaint( Color.BLUE );
		g2.setStroke( new BasicStroke( 5.0f ));
		for ( int i = 0; i < blue.size(); i++ ){
			g2.drawRect((int)blue.get(i).getX()+10,(int)blue.get(i).getY()+10,(int)blue.get(i).getWidth()-20,(int)blue.get(i).getHeight()-20);
		}
		g2.setPaint( Color.RED );
		g2.setStroke( new BasicStroke( 5.0f ));
		for ( int i = 0; i < red.size(); i++ ){
			g2.drawRect((int)red.get(i).getX()+10,(int)red.get(i).getY()+10,(int)red.get(i).getWidth()-20,(int)red.get(i).getHeight()-20);
		}
	}
	
	public static boolean getBit(int num,int index){
		return (num/((int)Math.pow(2, index)) & 1) == 1;
	}
	
}
