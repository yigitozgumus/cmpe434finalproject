import java.io.IOException;
import java.nio.channels.NetworkChannel;

import javax.naming.InitialContext;

import Group9.Globals.Actuator;
import Group9.Globals.Sensor;
import Group9.Map.GridMap;
import Group9.Network.Connection;
import Group9.Tasks.CreateMap;

public class Debug {

	public static void main(String[] args) throws IOException {
		Actuator.init();
		Connection.connect();
		CreateMap.run();
		/*while(true){
			System.out.println(Sensor.getColor());
		}*/
		/*for(int i=0; i<4; i++){
			Actuator.pilot.travel(80,false);
			Actuator.pilot.rotate(-90);
		}*/
		//Actuator.pilot.travel(100);
		
	}

	public static GridMap initialTest(){
		GridMap map = new GridMap();
		map.connect(1,1,2,1);
		map.connect(2,1,3,1);
		map.connect(3,1,4,1);
		map.connect(4,1,5,1);
		map.connect(3,1,3,2);
		map.connect(3,2,3,3);
		map.connect(3,3,2,3);
		map.connect(3,3,4,3);
		map.connect(4,3,4,4);
		map.connect(4,4,4,5);
		map.connect(4,5,5,5);
		map.connect(4,5,3,5);
		map.connect(3,5,2,5);
		map.connect(2,5,1,5);
		map.connect(1,5,1,4);
		map.connect(1,4,1,3);
		map.connect(1,3,1,2);
		map.connect(1,2,2,2);
		return map;
	}

}
