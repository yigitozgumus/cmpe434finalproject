package Group9.Map;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;

import javax.xml.bind.annotation.XmlInlineBinaryData;

import Group9.Globals.Dir;

public class GridMap {

	public boolean[][][][] connected = new boolean[12][12][12][12];
	
	float[][][] map = new float[4][12][12];

	public int bX=0,bY=0,rX=0,rY=0,mX=0,mY=0;
	
	public GridMap(){
		
	}
	
	public GridMap(String inFile){
		try {
			Scanner in = new Scanner(new File(inFile));	
			bX=in.nextInt();
			bY=in.nextInt();
			rX=in.nextInt();
			rY=in.nextInt();
			mX=in.nextInt();
			mY=in.nextInt();
			while(in.hasNext()){
				int x1=in.nextInt();
				int y1=in.nextInt();
				int x2=in.nextInt();
				int y2=in.nextInt();
				connect(x1,y1,x2,y2);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void writeToFile(String name){
		try {
			PrintWriter writer = new PrintWriter(new File(name));
			writer.println(bX);
			writer.println(bY);
			writer.println(rX);
			writer.println(rY);
			writer.println(mX);
			writer.println(mY);
			for(int x1=0; x1<12; x1++){
				for(int y1=0; y1<12; y1++){
					for(int x2=0; x2<x1; x2++){
						for(int y2=0; y2<y1; y2++){
							if(isConnected(x1,y1,x2,y2)){
								writer.println(x1+" "+y1+" "+x2+" "+y2);
							}
						}
					}
				}
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean[] getNeighbors(int x,int y,int heading){
		boolean[] result = new boolean[4];
		for(int i=heading; i<heading+4; i++){
			for(int j=0; j<8; j+=2){
				result[j/2] = isConnected(x, y, 
						x+Dir.headingTable[i%4][j] ,y+Dir.headingTable[i%4][j+1]);
			}
		}
		return result;
	}
	
	public int[] getPath(int xs,int ys,int xt,int yt,int heading){
		int[][] path = new int[12][12];
		for (int i = 0; i < path.length; i++) {
			for (int j = 0; j < path[i].length; j++) {
				path[i][j] = 5000;
			}
		}
		path[xs][ys]=0;
		LinkedList<State> q = new LinkedList<>();
		State cur = new State(xs,ys,null,heading,-1);
		q.addLast(cur);
		while(!q.isEmpty()){
			cur = q.removeFirst();
			int x=cur.x,y=cur.y,h=cur.h;
			if(x==xt && y==yt) break;
			for(int i=0 ; i<8; i+=2){
				int x2=x+Dir.headingTable[h][i];
				int y2=y+Dir.headingTable[h][i+1];
				if(isConnected(x, y, x2, y2) && path[x2][y2] > path[x][y]+1
						&& x2!=bX && y2!=bY ){
					path[x2][y2] = path[x][y]+1;
					q.addLast(new State(x2,y2,cur,(h+3+i/2)%4,i/2));
				}
			}
		}
		int[] reversed = new int[144];
		int size=0;
		while(cur.prev!=null){
			reversed[size++] = cur.c;
			cur=cur.prev;
		}		
		int[] result = new int[size];
		for(int i=0; i<size; i++){
			result[i]=reversed[size-1-i];
		}
		return result;
	}
	
	public boolean isConnected(int x1,int y1,int x2,int y2){
		return connected[x1][y1][x2][y2];
	}
	
	public void connect(int x1,int y1,int x2,int y2){
		connected[x1][y1][x2][y2]=true;
		connected[x2][y2][x1][y1]=true;
	}
	

	private class State{
		public int x,y,h,c;
		public  State prev;
		public State(int x,int y,State prev,int h,int c){
			this.x=x;
			this.y=y;
			this.h=h;
			this.c=c;
			this.prev=prev;
		}
	}
}