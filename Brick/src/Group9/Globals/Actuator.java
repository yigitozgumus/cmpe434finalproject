package Group9.Globals;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.Delay;

public class Actuator {

	private static int curPos = Dir.UP;
	
	
	//motors
	public static EV3LargeRegulatedMotor leftMotor = new EV3LargeRegulatedMotor(MotorPort.A);
	public static EV3LargeRegulatedMotor rightMotor = new EV3LargeRegulatedMotor(MotorPort.D);
	private static EV3LargeRegulatedMotor cageMotor = new EV3LargeRegulatedMotor(MotorPort.C);
	private static Chassis chassis;
	
	
	public 	static MovePilot pilot;
	public static EV3MediumRegulatedMotor mediumRegulatedMotor = new EV3MediumRegulatedMotor(MotorPort.B); 
	
	public static void init(){
		float leftDiameter = 5.598f;
		float rightDiameter = 5.617f;
    	float trackWidth = 11.194f;
    	boolean reverse = true;
    	chassis = new WheeledChassis(
    		new Wheel[]{
    				WheeledChassis.modelWheel(leftMotor,leftDiameter).offset(-trackWidth/2).invert(reverse),
    				WheeledChassis.modelWheel(rightMotor,rightDiameter).offset(trackWidth/2).invert(reverse)}, 
    		WheeledChassis.TYPE_DIFFERENTIAL);

    	pilot = new MovePilot(chassis);
    	setLinearSpeed(10);
    	setAngularSpeed(40);
    	pilot.stop();
    	cageMotor.backward();
    	//mediumRegulatedMotor.setSpeed(mediumRegulatedMotor.getMaxSpeed());
	}

	public static double getLinearSpeed() {
		return pilot.getLinearSpeed();
	}

	public static void setLinearSpeed(double linearSpeed) {
    	pilot.setLinearSpeed(linearSpeed);
	}

	public static double getAngularSpeed() {
		return pilot.getAngularSpeed();
	}

	public static void setAngularSpeed(double angularSpeed) {
    	pilot.setAngularSpeed(angularSpeed);
	}
	
	public static void rotateUltrasonic(int pos){
		if(pos>3 || pos<0) throw new IllegalArgumentException();
		mediumRegulatedMotor.rotate(Dir.posTable[curPos][pos],false);
		curPos=pos;
	}
	public static void rotateRobot(int dir){
		Dir.heading = Dir.rotationTable[Dir.heading][dir];
		double degree = Dir.rotateTable[dir];
		Sensor.resetGyro();
		if(dir==3){
			float left = Sensor.readUltrasonic(0);
			if(left-Sensor.readUltrasonic(2)>0)
				degree=-1*degree;
			rotateUltrasonic(1);
			Delay.msDelay(1000);
		}
		pilot.rotate(degree,false);
		double difference = - Sensor.readGyro() + degree;
		//System.out.println(difference);
		pilot.rotate(difference,false);
	}
	public static void closeCage(){
		
	}
	public static void openCage(){
		
	}

}