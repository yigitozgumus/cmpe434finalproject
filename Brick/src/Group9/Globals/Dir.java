package Group9.Globals;

public class Dir {

	public static int x,y,heading;
	
	public static double[] rotateTable = {
		-90,0,90,180	
	};
	
	public static final int LEFT = 0,
							UP = 1,
							RIGHT = 2,
							DOWN = 3;
	
	public static final int[][] posTable= {
			{0, 90, 180, 270},
			{-90, 0, 90, 180},
			{-180, -90, 0, 90},
			{-270,-180, -90, 0}
	};
	
	public  static final int[][] headingTable={
			{1,0, 0,-1, -1,0, 0,1},
			{0,-1, -1,0, 0,1, 1,0},
			{-1,0, 0,1, 1,0, 0,-1},
			{0,1, 1,0, 0,-1, -1,0}
	};
	
	public static final int[][] ultrasonicTable={
			{1,2,3,0},
			{0,1,2,3},
			{3,0,1,2},
			{2,3,0,1}
	};
	
	public static final int[][] rotationTable={
			{3,0,1,2},
			{0,1,2,3},
			{1,2,3,0},
			{2,3,0,1}
	};
	
}
