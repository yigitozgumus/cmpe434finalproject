package Group9.Globals;

import lejos.hardware.BrickFinder;
import lejos.hardware.ev3.EV3;
import lejos.hardware.lcd.GraphicsLCD;

public class LCD {
	private static EV3 ev3 = (EV3) BrickFinder.getDefault();
	private static GraphicsLCD graphicsLCD = ev3.getGraphicsLCD();
	
	public static void print(String text,int x,int y){
		graphicsLCD.drawString(text, graphicsLCD.getWidth()/2+x, graphicsLCD.getHeight()/2+y,
				GraphicsLCD.VCENTER|GraphicsLCD.HCENTER);
	}
	public static void clear(){
		graphicsLCD.clear();
	}
	public static void refresh(){
		graphicsLCD.refresh();
	}
	
	public static void printList(String[] texts){
		int offset = texts.length % 2 == 0 ?
				texts.length/2*(-30) : texts.length/2*(-20);
		LCD.clear();
		for(int i=0; i<texts.length; i++){
			LCD.print(texts[i], 0, offset+i*20);
			LCD.refresh();
		}
	}
}