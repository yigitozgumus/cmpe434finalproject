package Group9.Globals;

import Group9.Behaviors.Explore;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.NXTColorSensor;
import lejos.hardware.sensor.NXTUltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.ColorAdapter;
import lejos.robotics.LightDetectorAdaptor;
import lejos.robotics.SampleProvider;
import lejos.robotics.TouchAdapter;
import lejos.utility.Delay;

public class Sensor {
	//Sensors
//	private static NXTColorSensor frontColor = new NXTColorSensor(SensorPort.S3);
	private static EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S4);
	private static EV3UltrasonicSensor ultrasonicSensor = new EV3UltrasonicSensor(SensorPort.S2);
	private static NXTUltrasonicSensor frontUltrasonic = new NXTUltrasonicSensor(SensorPort.S1);
	private static EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S3);
	
	//Helpers
	private static ColorAdapter colorAdapter = new ColorAdapter(colorSensor);
	private static LightDetectorAdaptor lightDetectorAdaptor = new LightDetectorAdaptor((SampleProvider)colorSensor);
	private static SampleProvider sampleProviderUltrasonic = ultrasonicSensor.getDistanceMode();
	private static SampleProvider sampleFrontUltrasonic = frontUltrasonic.getDistanceMode();
	private static SampleProvider sampleProviderGyro = gyroSensor.getAngleMode();
	
	public static float readUltrasonic(){
	    float [] sample = new float[sampleProviderUltrasonic.sampleSize()];
    	sampleProviderUltrasonic.fetchSample(sample, 0);
		return sample[0]*100;
	}
	
	public static float readFrontUltrasonic(){
		float [] sample = new float[sampleFrontUltrasonic.sampleSize()];
		sampleFrontUltrasonic.fetchSample(sample, 0);
		return sample[0] *100;
	}
	public  static int getRed(){
		return colorAdapter.getColor().getRed();
	}
	public  static int getBlue(){
		return colorAdapter.getColor().getBlue();
	}
	public  static int getGreen(){
		return colorAdapter.getColor().getGreen();
	}
	public static float getLight(){
		return lightDetectorAdaptor.getLightValue();
	}
	public static float readGyro(){
		float [] sample = new float[sampleProviderGyro.sampleSize()];
    	sampleProviderGyro.fetchSample(sample, 0);
    	//System.out.println(sample[0]);
    	return sample[0];
	}
	public static void resetGyro(){
		gyroSensor.reset();
	} 
	public static int getColor(){ // 0 for black, 1 for red, 2 for blue, 3 for white
		Color color = colorAdapter.getColor();
		int red = color.getRed();
		int blue = color.getBlue();
		int green = color.getGreen();
		if(red >30  && blue >30  && green >30 ){
			return 3;
		}else if(red > 8 * blue){
			return 1;
		}else if(blue + green > 5 * red + 2){
			return 2;
		}else if(red < 12 && blue < 12 && green < 12){
			return 0;
		}
		return -1;		
	}

	/*public static float readUltrasonicRel(int dir){
		Actuator.rotateUltrasonic(dir);
		Delay.msDelay(1100);
		return readUltrasonic();
	}*/
	
	public static float readUltrasonic(int dir){
		//int relativeDir = Dir.ultrasonicTable[Dir.heading][dir];
		//System.out.println("heading: "+Dir.heading+" relative: "+relativeDir+" dir: "+dir);
		Actuator.rotateUltrasonic(dir);
		Delay.msDelay(1200);
		return readUltrasonic();
	}
}
