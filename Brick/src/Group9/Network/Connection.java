package Group9.Network;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import Group9.Globals.LCD;

public class Connection {
	public static ServerSocket serverSocket;
	public static Socket client;
	static OutputStream outputStream;
	static DataOutputStream dataOutputStream ;
	
	public static void connect() throws IOException{
		serverSocket = new ServerSocket(666);
		LCD.clear();
		LCD.print("Waiting Connection", 0, 0);
		LCD.refresh();
		client = serverSocket.accept();
		LCD.clear();
		LCD.print("Connected", 0, 0);
		LCD.refresh();
		outputStream = client.getOutputStream();
		dataOutputStream = new DataOutputStream(outputStream);
	}
	

	
	public static void send(int x,int y,int wall){
		//sendPos(x, y);
		try {
			dataOutputStream.writeInt(x);
			dataOutputStream.flush();
			dataOutputStream.writeInt(y);
			dataOutputStream.flush();
			dataOutputStream.writeInt(wall);
			dataOutputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
}
