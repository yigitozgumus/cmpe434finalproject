package Group9.Tasks;

import Group9.Behaviors.*;
import Group9.Globals.Dir;
import Group9.Map.GridMap;
import lejos.robotics.subsumption.Arbitrator;
import lejos.robotics.subsumption.Behavior;

public class CreateMap {
	
	public static GridMap map = new GridMap();
	
	public static void run(){
		Dir.x=6;
		Dir.y=6;
		Dir.heading=1;
		Behavior explore = new Explore();
		Behavior turnBackMortal = new TurnBackMortal();
		Behavior writeMap = new WriteMap();
		((Explore)explore).initCur(Dir.x,Dir.y);
		Explore.stack.push(((Explore)explore).cur);
		Behavior[] behaviors = {explore,turnBackMortal,writeMap};
		(new Arbitrator(behaviors)).go();
	}
	
}
