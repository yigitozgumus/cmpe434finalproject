package Group9.Behaviors;

import Group9.Tasks.CreateMap;
import lejos.robotics.subsumption.Behavior;

public class WriteMap implements Behavior{

	@Override
	public boolean takeControl() {
		return Explore.missionComplete;
	}

	@Override
	public void action() {
		CreateMap.map.writeToFile("map");
	}

	@Override
	public void suppress() {
		
	}

}
