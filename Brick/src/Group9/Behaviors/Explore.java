package Group9.Behaviors;

import java.util.Stack;

import Group9.Globals.Actuator;
import Group9.Globals.Dir;
import Group9.Globals.Sensor;
import Group9.Network.Connection;
import Group9.Tasks.CreateMap;
import lejos.hardware.port.SensorPort;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

public class Explore implements Behavior {

	public static boolean missionComplete=false;
	
	public static final float topla=11.75f;
	private boolean suppressed=false;
	public static final float TRESHOLD = 27f;  
	public static Stack<Pair> stack = new Stack<>();
	public static boolean[][] visited =new boolean[12][12];
	float left=50,right=50; // Dikaat Hocam!
	public Pair cur;
	@Override
	public boolean takeControl() {
		return !missionComplete;
	}

	public void initCur(int x,int y){
		cur=new Pair(x, y);
	}
	
	@Override
	public void action() {
		suppressed=false;
		while(!stack.isEmpty() && !suppressed){
			cur = stack.pop();
			int x=cur.x,y=cur.y;
			System.out.println(x+" "+y);
			visited[x][y]=true;
			toplaGel();
			moveTo(x,y);
			if(suppressed) break;
			int wallData=0;
			if(Sensor.getColor()==2){
				CreateMap.map.mX=Dir.x; 
				CreateMap.map.mY=Dir.y;
				wallData+=(int)Math.pow(2, 5);
			}
			if(Sensor.getColor()==1){
				CreateMap.map.rX=Dir.x; 
				CreateMap.map.rY=Dir.y;
				wallData+=(int)Math.pow(2,4);
			}
			for(int i=0; i<4 && !suppressed; i++){
				int relativeDir = Dir.ultrasonicTable[Dir.heading][i];
				float val = Sensor.readUltrasonic(relativeDir);
				//System.out.println("ultrasonic value: "+val);
				if(relativeDir==0) left=val;
				else if( relativeDir==2) right=val;
				if(val > TRESHOLD ) {
					int x2=x+Dir.headingTable[1][i*2];
					int y2=y+Dir.headingTable[1][i*2+1];
					//System.out.println(i+" x:"+x2+" y:"+y2);
					CreateMap.map.connect(x, y, x2, y2);
					if(!visited[x2][y2]){
						stack.push(new Pair(x2,y2));
					}
				}else{
					wallData+=(int)Math.pow(2, i);
				}
			}
			System.out.println("wall data: "+wallData);
			Actuator.rotateUltrasonic(1);
			Connection.send(Dir.x, Dir.y, wallData);
			Delay.msDelay(1000);
		}
		missionComplete=stack.isEmpty();
	}

	private void moveTo(int x, int y) {
		int[] commands = CreateMap.map.getPath(Dir.x, Dir.y, x, y, Dir.heading);
		for(int i=0; i<commands.length; i++){
			if(i!=0 && i!=commands.length-1){
				left=Sensor.readUltrasonic(0);
				right = Sensor.readUltrasonic(2);
				toplaGel();
				Actuator.rotateUltrasonic(1);
				Connection.send(Dir.x, Dir.y, 0);
				Delay.msDelay(1000);
			}
			Dir.x=Dir.x+Dir.headingTable[Dir.heading][commands[i]*2];
			Dir.y=Dir.y+Dir.headingTable[Dir.heading][commands[i]*2+1];
			if(commands[i]!=1)
				Actuator.rotateRobot(commands[i]);
			Actuator.pilot.travel(33,true);
			while(Actuator.pilot.isMoving() && Sensor.readUltrasonic() > 15.5f){
				Thread.yield();
			}
			Actuator.pilot.stop();
		}
		
	}

	public void toplaGel(){
		//if(!topla) return;
		Sensor.resetGyro();
		if(right<TRESHOLD){
			if(right<topla){
				solTopla();
			}else if(right>33-topla){
				sagTopla();
			}
		}else if(left<TRESHOLD){
			if(left<topla){
				sagTopla();
			}else if(left>33-topla){
				solTopla();
			}
		}
		Actuator.pilot.rotate(-1*Sensor.readGyro());
	}
	
	public static void solTopla(){
		Actuator.rightMotor.rotate(-220,false);
		Actuator.leftMotor.rotate(-220,false);
		Actuator.rightMotor.rotate(220,true);
		Actuator.leftMotor.rotate(220,false);
	}
	
	public static void sagTopla(){
		Actuator.leftMotor.rotate(-220,false);
		Actuator.rightMotor.rotate(-220,false);
		Actuator.rightMotor.rotate(220,true);
		Actuator.leftMotor.rotate(220,false);
	}
	
	@Override
	public void suppress() {
		suppressed=true;
	}

	class Pair{
		public int x,y;
		public Pair(int x,int y){
			this.x=x; this.y=y;
		}
	}
	
}
