package Group9.Behaviors;

import Group9.Globals.Actuator;
import Group9.Globals.Dir;
import Group9.Globals.Sensor;
import Group9.Network.Connection;
import Group9.Tasks.CreateMap;
import lejos.robotics.subsumption.Behavior;
import lejos.utility.Delay;

public class TurnBackMortal implements Behavior {

	@Override
	public boolean takeControl() {
		return Sensor.getColor()==0;
	}

	@Override
	public void action() {
		CreateMap.map.bX=Dir.x;
		CreateMap.map.bY=Dir.y;
		Connection.send(Dir.x, Dir.y, 64);
		System.out.println("siyahci");
		Actuator.rotateUltrasonic(3);
		Delay.msDelay(1000);
		Actuator.leftMotor.rotate(746,true);
		Actuator.rightMotor.rotate(746,true);
		while(Actuator.leftMotor.isMoving() && Actuator.rightMotor.isMoving() && Sensor.readUltrasonic() > 14 ){
			Thread.yield();
		}
		Actuator.pilot.stop();
		Dir.x = Dir.x-Dir.headingTable[Dir.heading][6];
		Dir.y = Dir.y-Dir.headingTable[Dir.heading][7];
		Actuator.rotateUltrasonic(1);
		Delay.msDelay(1000);
	
	}

	@Override
	public void suppress() {
		// TODO Auto-generated method stub
		
	}

}
