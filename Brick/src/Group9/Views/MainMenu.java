package Group9.Views;

import Group9.Globals.LCD;

public class MainMenu {

	public static void printMenu(){
		String texts[] = {
				"Usage",
				"UP -> CreateMap",
				"Down -> ServeFood",
				"Enter -> Idle",
				"Escape -> Reset "
		};
		LCD.printList(texts);
	}
	
}
